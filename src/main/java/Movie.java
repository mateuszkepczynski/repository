import java.util.Date;

/**
 * Created by RENT on 2017-08-16.
 */
public class Movie {
   String title;
   MovieType movieType;
   Date releaseDate;
   String author;

    public Movie(String title, MovieType movieType, Date releaseDate, String author) {
        this.title = title;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", movieType=" + movieType +
                ", releaseDate=" + releaseDate +
                ", author='" + author + '\'' +
                '}';
    }
}
