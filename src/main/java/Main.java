import sun.util.resources.cldr.lag.CalendarData_lag_TZ;

import java.sql.Date;

/**
 * Created by RENT on 2017-08-16.
 */
public class Main {
    public static void main(String[] args) {
        MoviesDatabase moviesDatabase = new MoviesDatabase();

        moviesDatabase.addMovie(new Movie("Karolina która programuje", MovieType.COMEDY, Date.valueOf("11-01-2016"), "Dean Koontz"));
        moviesDatabase.addMovie(new Movie("Mateusz który programuje", MovieType.HORROR, Date.valueOf("11-01-2014"), "Stephen King"));
        System.out.println(moviesDatabase.searchMovie("Mateusz który programuje"));
        moviesDatabase.printAllMovies();

        ////2. Stwórz maina w którym będziesz testować tą funkcjonalność dodawania/wyszukiwania/wypisywania
        // i filtrowania (wypisywanie tylko tych z wybranej kategorii) filmów.
    }
}
